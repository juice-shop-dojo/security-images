
# Guide we made the application from

[Microsoft guide](https://learn.microsoft.com/en-us/microsoft-365/security/defender-endpoint/api/api-hello-world?view=o365-worldwide)

Go to azure portal->App registrations->New registration
We need to add API Permission (on the left side): MicrosoftDefenderATP 

# Using env variables
Don't blame us for this variables naming, that's how Microsoft calls them


APP_CLIENT_ID = key from the instruction above 

TENANT_ID = key from the instruction above

SECRET_ID_VALUE = key from the instruction above

REPORT_FILE_NAME = defender_report.json

REPORT_FILE_PATH = /data/

DAYS=30 (timedelta for alerts)

# how to run

python /main.py