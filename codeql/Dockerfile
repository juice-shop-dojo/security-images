FROM openjdk:17-slim

# Update and install necessary dependencies
RUN apt-get update && \
    apt-get install -y wget curl unzip git build-essential

# Create 'whitespots' user and home directory
RUN useradd -ms /bin/bash whitespots && \
    mkdir -p /home/whitespots && \
    chown -R whitespots:whitespots /home/whitespots
USER whitespots

# Set up environment variables for Golang
ENV GOLANG_VERSION 1.18
ENV GOROOT /home/whitespots/go
ENV GOPATH /home/whitespots/work
ENV PATH $GOPATH/bin:$GOROOT/bin:$PATH

# Install Golang
USER root
RUN curl -LO https://golang.org/dl/go${GOLANG_VERSION}.linux-amd64.tar.gz && \
    tar -C /home/whitespots -xzf go${GOLANG_VERSION}.linux-amd64.tar.gz && \
    rm go${GOLANG_VERSION}.linux-amd64.tar.gz && \
    chown -R whitespots:whitespots /home/whitespots/go
USER whitespots

# Install Gradle
ENV GRADLE_VERSION 7.2
ENV GRADLE_HOME /home/whitespots/gradle
RUN mkdir /home/whitespots/gradle && \
    curl -sL -o /home/whitespots/gradle/gradle-${GRADLE_VERSION}-bin.zip https://services.gradle.org/distributions/gradle-${GRADLE_VERSION}-bin.zip && \
    unzip -d /home/whitespots/gradle /home/whitespots/gradle/gradle-${GRADLE_VERSION}-bin.zip && \
    rm /home/whitespots/gradle/gradle-${GRADLE_VERSION}-bin.zip
ENV PATH ${GRADLE_HOME}/gradle-${GRADLE_VERSION}/bin:$PATH


# Install Python, Node.js and other necessary libraries for CodeQL languages
USER root
RUN apt-get install -y python3 python3-pip nodejs npm libssl-dev

# Install CodeQL CLI
ENV SEC_CODEQL_VER 2.12.4
ENV CODEQL_CLI_ARCHIVE codeql-linux64.zip
ENV CODEQL_CLI_DOWNLOAD_URL https://github.com/github/codeql-cli-binaries/releases/download/v${SEC_CODEQL_VER}/${CODEQL_CLI_ARCHIVE}
RUN curl -sL -o /tmp/${CODEQL_CLI_ARCHIVE} ${CODEQL_CLI_DOWNLOAD_URL} && \
    unzip /tmp/${CODEQL_CLI_ARCHIVE} -d /opt && \
    rm /tmp/${CODEQL_CLI_ARCHIVE}
ENV PATH /opt/codeql:${PATH}

# Set the user is impossible for now..
# USER whitespots

RUN git clone https://github.com/github/codeql /opt/codeql-repo

ENTRYPOINT []