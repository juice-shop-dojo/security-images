import os
import requests
import json

# Don't blame us for this variables naming, that's how Microsoft calls them
client_id = os.environ.get('APP_CLIENT_ID', '1111111')
client_secret = os.environ.get('SECRET_ID_VALUE', '222222222')
tenant_id = os.environ.get('TENANT_ID', '3333333333')
report_file_name = os.environ.get('REPORT_FILE_NAME', 'microsoft_graph_report.json')
report_file_path = os.environ.get('REPORT_FILE_PATH', '/data/')
token = ''

# Replace with your own values
resource_app_id_uri = 'https://graph.microsoft.com'

# Define the OAuth URL
oauth_url = f'https://login.microsoftonline.com/{tenant_id}/oauth2/token'

# Prepare the request body
auth_body = {
    'resource': resource_app_id_uri,
    'client_id': client_id,
    'client_secret': client_secret,
    'grant_type': 'client_credentials'
}

# Make a POST request to acquire the access token
auth_response = requests.post(oauth_url, data=auth_body)

if auth_response.status_code == 200:
    auth_response_data = auth_response.json()
    token = auth_response_data['access_token']
else:
    print(f'Failed to retrieve the access token. Status code: {auth_response.status_code}')


url_riskyUsers = f"https://graph.microsoft.com/beta/riskyUsers"
url_riskyServices = f"https://graph.microsoft.com/beta/identityProtection/riskyServicePrincipals"

# Set the request headers
headers = {
    'Content-Type': 'application/json',
    'Accept': 'application/json',
    'Authorization': f'Bearer {token}'
}

alerts = list()
clean_alerts = list()

try:
    # Send the HTTP GET request and get the results
    response = requests.get(url_riskyUsers, headers=headers)

    if response.status_code == 200:
        alerts = response.json()['value']
        for alert in alerts:
            if alert.get('riskDetail') != 'none':
                clean_alerts.append(alert)
    else:
        print(f"Failed to retrieve alerts. Status code: {response.status_code}")
except Exception as e:
    print(f"An error occurred: {str(e)}")

try:
    # Send the HTTP GET request and get the results
    response = requests.get(url_riskyServices, headers=headers)

    if response.status_code == 200:
        alerts = response.json()['value']
        for alert in alerts:
            if alert.get('riskDetail') != 'none':
                clean_alerts.append(alert)
    else:
        print(f"Failed to retrieve alerts. Status code: {response.status_code}")
except Exception as e:
    print(f"An error occurred: {str(e)}")

#####################################################
#   Now let's put the data in a right formet
#####################################################

portal_findings_list = list()

for alert in clean_alerts:
    data = dict()
    title = 'Risky entity detected: '
    title += alert.get('displayName') if 'displayName' in alert else alert.get('userDisplayName')
    data['name'] = f'[RiskyUsersAndApps] {title}'

    updated_time = alert.get('riskLastUpdatedDateTime', 'Unparsed date')
    description = f"""
        [{updated_time}] 
        riskDetail: {alert.get('riskDetail', 'Unable to parse')}
        displayName: {alert.get('displayName', 'Unable to parse')}
        userDisplayName: {alert.get('userDisplayName', 'Unable to parse')}
        appId: {alert.get('appId', 'Unable to parse')}
        
    """
    data['description'] = description

    severity = alert.get('riskLevel', 'Low').capitalize()
    data['severity'] = severity

    data['vulnerable_url'] = 'https://security.microsoft.com/user?aad=' + alert.get('id', '1111111111')
    portal_findings_list.append(data)

# Specify the file path
file_path = report_file_path + report_file_name

# Write the list of dictionaries to the file
with open(file_path, "w") as json_file:
    json.dump(portal_findings_list, json_file, indent=4)

print(f"Data has been written to {file_path}")
